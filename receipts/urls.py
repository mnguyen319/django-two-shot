from django.urls import path
from receipts.views import (
    ReceiptCreateView,
    ReceiptListView,
    ExpenseCategoryListView,
    ExpenseCategoryCreateView,
    AccountCreateView,
    AccountListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "categories/", ExpenseCategoryListView.as_view(), name="expense_list"
    ),
    path("create/", ReceiptCreateView.as_view(), name="receipt_new"),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_expense",
    ),
    path("accounts/", AccountListView.as_view(), name="account_view"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
]
