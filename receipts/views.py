from django.shortcuts import redirect, render
from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from receipts.models import Account, ExpenseCategory, Receipt


class ReceiptListView(ListView, LoginRequiredMixin):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    template_name = "receipts/list.html"
    context_object_name = "receipts"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    template_name = "receipts/new.html"

    def form_valid(self, form):
        item = form.save(commit=False)  # setting up, but not actually saving
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    fields = ["name", "number"]
    template_name = "receipts/account_new.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("home")


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accounts_list.html"
    fields = ["name", "number"]
    context_object_name = "accounts"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = ["name"]
    template_name = "receipts/expense_new.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("home")


class ExpenseCategoryListView(ListView, LoginRequiredMixin):
    template_name = "receipts/expense_list.html"
    model = ExpenseCategory
    context_object_name = "expenses"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)
