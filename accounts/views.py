from re import template
from django.shortcuts import redirect, render
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login


class AccountsLoginView(LoginView):
    template_name = "login.html"


class AccountsLogoutView(LogoutView):
    template_name = "logged_out.html"


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password1"]
            user = User.objects.create_user("username", "password")
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm(request.POST)
    context = {"form": form}
    return render(request, "registration/signup.html", context)
